# freepager

#### 项目介绍
- 项目名称：freepager
- 所属系列：openharmony的第三方组件适配移植
- 功能：视图分页器
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：Releases v1.0

#### 效果演示
<img src="img/demo.gif"></img>

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:freepager:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

```示例XML
<DirectionalLayout
    xmlns:ohos="http://schemas.huawei.com/res/ohos"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="#E6E6E6"
    ohos:orientation="vertical">

    <PageSlider
        ohos:id="$+id:pager"
        ohos:height="match_parent"
        ohos:width="match_parent"
        ohos:background_element="#E6E6E6"/>
</DirectionalLayout>
```

```java

public class FixedViewsFraction extends Fraction implements ViewFactory {

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fraction_fixed_views, container, false);
        PageSlider pageSlider = (PageSlider) component.findComponentById(ResourceTable.Id_pager);
        pageSlider.setProvider(new InfiniteVerticalPagerAdapter(this, 0));
        pageSlider.setCurrentPage(0);
        pageSlider.setOrientation(Component.VERTICAL);
        pageSlider.setReboundEffect(true);
        pageSlider.setCentralScrollMode(true);
        return component;
    }

    @Override
    public Component makeView(int vertical, int horizontal) {
        Button btn = new Button(this);
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                DirectionalLayout.LayoutConfig.MATCH_PARENT);
        btn.setLayoutConfig(layoutConfig);
        btn.setText("VERTICAL " + horizontal);
        btn.setTextSize(40);
        Font.Builder builder = new Font.Builder(btn.getText()).setWeight(600);
        btn.setFont(builder.build());
        return btn;
    }
}
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

    Copyright 2015, Alex Zaitsev
    
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at
    
       http://www.apache.org/licenses/LICENSE-2.0
    
    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
