## 1.0.0
ohos 第二个版本
* 正式版本
* 因为鸿蒙暂不支持事件分发机制，View的computeScroll方法等方法，暂用pageslider自身属性实现，滑动方向横竖切换时会有问题

## 0.0.1-SNAPSHOT
ohos第一个版本，完整实现了原库的全部api