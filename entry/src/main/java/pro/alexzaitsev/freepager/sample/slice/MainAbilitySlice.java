package pro.alexzaitsev.freepager.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.window.service.WindowManager;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import pro.alexzaitsev.freepager.sample.*;
import pro.alexzaitsev.freepager.sample.adapter.ListAdapter;
import pro.alexzaitsev.freepager.sample.adapter.ViewHolder;

import java.io.IOException;
import java.util.Arrays;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    public static final int POSITION_FIXED = 0;
    public static final int POSITION_VERTICAL = 1;
    public static final int POSITION_HORIZONTAL = 2;
    public static final int POSITION_FREE = 3;
    private Fraction mCurrentFraction;
    private ListAdapter listAdapter;
    private int mCurrentFragmentPosition = -1;
    private String[] mTitles;
    private Text titleText;

    @Override
    public void onStart(Intent intent) {
        // 设置状态栏颜色
        WindowManager.getInstance().getTopWindow().get().setStatusBarColor(getColor(ResourceTable.Color_black));
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        initViews();
    }

    private void initViews() {
        titleText = (Text) findComponentById(ResourceTable.Id_title);
        Image aboutImage = (Image) findComponentById(ResourceTable.Id_about);
        aboutImage.setClickedListener(this);
        ListContainer listContainer = (ListContainer) findComponentById(ResourceTable.Id_list_main);
        try {
            mTitles = getResourceManager().getElement(ResourceTable.Strarray_main).getStringArray();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
        listAdapter = new ListAdapter<String>(this, ResourceTable.Layout_list_item, Arrays.asList(mTitles)) {
            @Override
            public void convert(ViewHolder viewHolder, String item, int position) {
                Text text = viewHolder.getView(ResourceTable.Id_item_title);
                text.setText(item);
            }
        };
        listContainer.setItemProvider(listAdapter);
        listContainer.setItemClickedListener(new ListContainer.ItemClickedListener() {
            @Override
            public void onItemClicked(ListContainer listContainer, Component component, int i, long l) {
                switch (i) {
                    case POSITION_FIXED:
                        mCurrentFraction = new FixedViewsFraction();
                        break;

                    case POSITION_VERTICAL:
                        mCurrentFraction = new InfiniteVerticalFragment();
                        break;

                    case POSITION_HORIZONTAL:
                        mCurrentFraction = new InfiniteHorizontalFraction();
                        break;

                    case POSITION_FREE:
                        mCurrentFraction = new FreeFraction();
                        break;

                    default:
                        break;
                }
                ((MainAbility) getAbility()).getFractionManager()
                        .startFractionScheduler()
                        .add(ResourceTable.Id_fraction, mCurrentFraction)
                        .submit();
                mCurrentFragmentPosition = i;
                setTitle(mTitles[mCurrentFragmentPosition]);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_about:
                Intent intent = new Intent();
                present(new AboutAbilitySlice(), intent);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onBackPressed() {
        if (mCurrentFraction == null) {
            super.onBackPressed();
        } else {
            ((MainAbility) getAbility()).getFractionManager()
                    .startFractionScheduler()
                    .remove(mCurrentFraction)
                    .submit();
            mCurrentFraction = null;
            mCurrentFragmentPosition = -1;
            setTitle(getString(ResourceTable.String_app_name));
        }
    }

    private void setTitle(String title) {
        titleText.setText(title);
    }
}
