package pro.alexzaitsev.freepager.sample;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import pro.alexzaitsev.freepager.library.view.infinite.InfiniteHorizontalPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Version 1.0
 * ModifiedBy
 * date 2021-05-24 19:50
 * description 横向滑动的pageslide
 */
public class InfiniteHorizontalFraction extends Fraction implements PageSlider.PageChangedListener {
    private static final int SIZE = 3;
    private PageSlider viewPager;
    private List<Component> views;
    private Button button;
    private int currentPage = 0;
    private int pageIndex = 0;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fraction_infinite_horizontal, container, false);
        initWidget(component);
        return component;
    }

    private void initWidget(Component component) {
        viewPager = (PageSlider) component.findComponentById(ResourceTable.Id_pager);

        views = new ArrayList<>();
        for (int i = 0; i < SIZE; i++) {
            Button button = new Button(this);
            DirectionalLayout.LayoutConfig layoutConfig =
                    new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                            DirectionalLayout.LayoutConfig.MATCH_PARENT);
            button.setLayoutConfig(layoutConfig);
            button.setTextSize(40);
            Font.Builder builder = new Font.Builder(button.getText()).setWeight(600);
            button.setFont(builder.build());
            ShapeElement shapeElement = new ShapeElement();
            button.setBackground(shapeElement);
            views.add(button);
        }
        initImageData();
        viewPager.setOrientation(Component.HORIZONTAL);
        viewPager.setCentralScrollMode(true);
        viewPager.setProvider(new InfiniteHorizontalPagerAdapter(views));
        viewPager.setCurrentPage(1);
        viewPager.addPageChangedListener(this);
    }


    private void initImageData() {
        for (int i = 0; i < SIZE; i++) {
            button = (Button) views.get(i);
            button.setText("HORIZONTAL " + (i - 1));
        }
    }

    @Override
    public void onPageSliding(int i, float v, int i1) {
    }

    @Override
    public void onPageSlideStateChanged(int state) {
        switch (state) {
            // 在滚动完成后
            case PageSlider.SCROLL_IDLE_STAGE:
                pageChange();
                break;
        }
    }

    @Override
    public void onPageChosen(int i) {
    }

    private void pageChange() {
        if (viewPager.getCurrentPage() == 1) {
            // 如果位置没有变终止循环
            return;
        }
        if (viewPager.getCurrentPage() > 1) {
            currentPage++;
            pageIndex++;
        } else {
            currentPage--;
            pageIndex--;
        }

        if (currentPage == SIZE) {
            currentPage = 0;
        }

        if (currentPage == -1) {
            currentPage = SIZE - 1;
        }
        Button zeroButton = (Button) views.get(0);
        zeroButton.setText("HORIZONTAL " + (pageIndex - 1));
        Button oneButton = (Button) views.get(1);
        oneButton.setText("HORIZONTAL " + pageIndex);
        Button twoButton = (Button) views.get(2);
        twoButton.setText("HORIZONTAL " + (pageIndex + 1));
        viewPager.setCurrentPage(1, false);
    }
}
