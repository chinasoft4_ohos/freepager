package pro.alexzaitsev.freepager.sample.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.utils.IntentConstants;
import ohos.utils.net.Uri;
import pro.alexzaitsev.freepager.sample.ResourceTable;

public class AboutAbilitySlice extends AbilitySlice implements Component.ClickedListener {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_about);
        initViews();
    }

    private void initViews() {
        findComponentById(ResourceTable.Id_layout_gitee_project).setClickedListener(this);
        findComponentById(ResourceTable.Id_layout_gitee_me).setClickedListener(this);
        findComponentById(ResourceTable.Id_layout_site_me).setClickedListener(this);
        findComponentById(ResourceTable.Id_layout_google_plus_me).setClickedListener(this);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onClick(Component component) {
        int id = component.getId();
        String url = null;
        if (id == ResourceTable.Id_layout_gitee_project) {
            url = getString(ResourceTable.String_gitee_project_link);
        } else if (id == ResourceTable.Id_layout_gitee_me) {
            url = getString(ResourceTable.String_gitee_me_link);
        } else if (id == ResourceTable.Id_layout_site_me) {
            url = getString(ResourceTable.String_site_me_link);
        } else if (id == ResourceTable.Id_layout_google_plus_me) {
            url = getString(ResourceTable.String_google_plus_me_link);
        }
        Intent intent = new Intent();
        Operation operation = new Intent.OperationBuilder()
                .withUri(Uri.parse(url))
                .withAction(IntentConstants.ACTION_SEARCH)
                .withBundleName("com.huawei.browser")
                .build();
        intent.setOperation(operation);
        startAbility(intent);
    }
}
