package pro.alexzaitsev.freepager.sample;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.LayoutAlignment;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * Version 1.0
 * ModifiedBy
 * date 2021-05-24 19:50
 * description 横向滑动的pageslide
 */
public class FreeFraction extends Fraction implements PageSlider.PageChangedListener {
    private static final int SIZE = 3;
    private PageSlider pageSlider;
    private List<DirectionalLayout> views;
    private int currentPage = 0;
    private float downX;    //按下时 的X坐标
    private float downY;    //按下时 的Y坐标
    private String moveDirection = "x";
    private int currentHerIndex = 0;
    private int currentVerIndex = 0;


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fraction_infinite_horizontal, container, false);
        initWidget(component);
        return component;
    }

    private void initWidget(Component component) {
        pageSlider = (PageSlider) component.findComponentById(ResourceTable.Id_pager);
        pageSlider.setOrientation(Component.VERTICAL);
        views = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            DirectionalLayout directionalLayout = new DirectionalLayout(getContext());
            DirectionalLayout.LayoutConfig layoutConfig =
                    new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                            DirectionalLayout.LayoutConfig.MATCH_PARENT);
            directionalLayout.setAlignment(LayoutAlignment.CENTER);
            directionalLayout.setLayoutConfig(layoutConfig);
            DirectionalLayout.LayoutConfig buttonConfig =
                    new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_CONTENT,
                            DirectionalLayout.LayoutConfig.MATCH_CONTENT);
            Button buttonUp = new Button(this);
            buttonUp.setLayoutConfig(buttonConfig);
            buttonUp.setTextSize(40);
            Font.Builder builder = new Font.Builder(buttonUp.getText()).setWeight(600);
            buttonUp.setFont(builder.build());
            ShapeElement shapeElement = new ShapeElement();
            buttonUp.setBackground(shapeElement);
            directionalLayout.setOrientation(Component.VERTICAL);
            directionalLayout.addComponent(buttonUp);
            Button buttonDown = new Button(this);
            buttonDown.setLayoutConfig(buttonConfig);
            buttonDown.setTextSize(40);
            buttonDown.setFont(builder.build());
            buttonDown.setBackground(shapeElement);
            directionalLayout.addComponent(buttonDown);
            views.add(directionalLayout);
        }
        initImageData();
        pageSlider.setCentralScrollMode(true);
        pageSlider.setProvider(new FreeFraction.PagerAdapter());
        pageSlider.setCurrentPage(1);
        pageSlider.addPageChangedListener(this);
        pageSlider.setTouchEventListener(new Component.TouchEventListener() {
            @Override
            public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
                int action = touchEvent.getAction();
                MmiPoint point = touchEvent.getPointerPosition(touchEvent.getIndex());
                //在触发时回去到起始坐标
                float x = point.getX();
                float y = point.getY();
                switch (action) {
                    case TouchEvent.PRIMARY_POINT_DOWN:
                        //将按下时的坐标存储
                        downX = x;
                        downY = y;
                        return true;
                    case TouchEvent.PRIMARY_POINT_UP:
                        //获取到距离差
                        float dx = x - downX;
                        float dy = y - downY;
                        //防止是按下也判断
                        if (Math.abs(dx) > 5 && Math.abs(dy) > 5) {
                            //通过距离差判断方向
                            int orientation = getOrientation(dx, dy);
                            switch (orientation) {
                                case 'r':
                                case 'l':
                                    pageSlider.setOrientation(Component.HORIZONTAL);
                                    break;
                                case 't':
                                case 'b':
                                    pageSlider.setOrientation(Component.VERTICAL);
                                    break;
                            }
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }
        });
    }


    /**
     * 根据距离差判断 滑动方向
     *
     * @param dx X轴的距离差
     * @param dy Y轴的距离差
     * @return 滑动的方向
     */
    private int getOrientation(float dx, float dy) {
        if (Math.abs(dx) > Math.abs(dy)) {
            //X轴移动
            moveDirection = "x";
            return dx > 0 ? 'r' : 'l';
        } else {
            //Y轴移动
            moveDirection = "y";
            return dy > 0 ? 'b' : 't';
        }
    }

    private void initImageData() {
        for (int i = 0; i < 3; i++) {
            DirectionalLayout directionalLayout = views.get(i);
            Button upButton = (Button) directionalLayout.getComponentAt(0);
            Button downButton = (Button) directionalLayout.getComponentAt(1);
            if (i == 0) {
                upButton.setText("HORIZONTAL " + 0);
                downButton.setText("VERTICAL " + 0);
            } else if (i == 1) {
                upButton.setText("HORIZONTAL " + 0);
                downButton.setText("VERTICAL " + 0);
            } else if (i == 2) {
                upButton.setText("HORIZONTAL " + 0);
                downButton.setText("VERTICAL " + 0);
            }
        }
    }

    @Override
    public void onPageSliding(int i, float v, int i1) {
    }

    @Override
    public void onPageSlideStateChanged(int state) {
        switch (state) {
            // 在滚动完成后
            case PageSlider.SCROLL_IDLE_STAGE:
                pageChange();
                break;
        }
    }

    @Override
    public void onPageChosen(int i) {
    }

    public class PagerAdapter extends PageSliderProvider {

        public PagerAdapter() {
        }

        @Override
        public int getCount() {
            return views.size();
        }

        @Override
        public Object createPageInContainer(ComponentContainer componentContainer, int position) {
            DirectionalLayout directionalLayout = views.get(position);
            componentContainer.removeComponent(directionalLayout);
            componentContainer.addComponent(directionalLayout);
            return directionalLayout;
        }

        @Override
        public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
            componentContainer.removeComponent((Component) o);
        }

        @Override
        public boolean isPageMatchToObject(Component component, Object object) {
            return component == object;
        }
    }

    private void pageChange() {
        if (pageSlider.getCurrentPage() == 1) {
            // 如果位置没有变终止循环
            return;
        }
        if (pageSlider.getCurrentPage() > 1) {
            currentPage++;
            if (moveDirection.equals("x")) {
                currentHerIndex++;
            } else {
                currentVerIndex++;
            }
        } else {
            currentPage--;
            if (moveDirection.equals("x")) {
                currentHerIndex--;
            } else {
                currentVerIndex--;
            }
        }
        if (currentPage == SIZE) {
            currentPage = 0;
        }
        if (currentPage == -1) {
            currentPage = SIZE - 1;
        }
        DirectionalLayout directionalLayoutZero = views.get(0);
        DirectionalLayout directionalLayoutOne = views.get(1);
        DirectionalLayout directionalLayoutTwo = views.get(2);
        if (moveDirection.equals("x")) {
            Button upZeroButton = (Button) directionalLayoutZero.getComponentAt(0);
            Button upOneButton = (Button) directionalLayoutOne.getComponentAt(0);
            Button upTwoButton = (Button) directionalLayoutTwo.getComponentAt(0);
            upZeroButton.setText("HORIZONTAL " + (currentHerIndex - 1));
            upOneButton.setText("HORIZONTAL " + currentHerIndex);
            upTwoButton.setText("HORIZONTAL " + (currentHerIndex + 1));
            Button downOneButton = (Button) directionalLayoutOne.getComponentAt(1);
            downOneButton.setText("VERTICAL " + currentVerIndex);
            Button downZeroButton = (Button) directionalLayoutZero.getComponentAt(1);
            downZeroButton.setText("VERTICAL " + currentVerIndex);
            Button downTwoButton = (Button) directionalLayoutTwo.getComponentAt(1);
            downTwoButton.setText("VERTICAL " + currentVerIndex);
        } else {
            Button downZeroButton = (Button) directionalLayoutZero.getComponentAt(1);
            Button downOneButton = (Button) directionalLayoutOne.getComponentAt(1);
            Button downTwoButton = (Button) directionalLayoutTwo.getComponentAt(1);
            downZeroButton.setText("VERTICAL " + (currentVerIndex - 1));
            downOneButton.setText("VERTICAL " + currentVerIndex);
            downTwoButton.setText("VERTICAL " + (currentVerIndex + 1));
            Button upOneButton = (Button) directionalLayoutOne.getComponentAt(0);
            upOneButton.setText("HORIZONTAL " + currentHerIndex);
            Button upZeroButton = (Button) directionalLayoutZero.getComponentAt(0);
            upZeroButton.setText("HORIZONTAL " + currentHerIndex);
            Button upTwoButton = (Button) directionalLayoutTwo.getComponentAt(0);
            upTwoButton.setText("HORIZONTAL " + currentHerIndex);
        }
        pageSlider.setCurrentPage(1, false);
    }
}
