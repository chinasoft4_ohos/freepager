package pro.alexzaitsev.freepager.sample;

import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.text.Font;
import pro.alexzaitsev.freepager.library.view.infinite.InfiniteVerticalPagerAdapter;
import pro.alexzaitsev.freepager.library.view.infinite.ViewFactory;

public class FixedViewsFraction extends Fraction implements ViewFactory {

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_fraction_fixed_views, container, false);
        PageSlider pageSlider = (PageSlider) component.findComponentById(ResourceTable.Id_pager);
        pageSlider.setProvider(new InfiniteVerticalPagerAdapter(this, 0));
        pageSlider.setCurrentPage(0);
        pageSlider.setOrientation(Component.VERTICAL);
        pageSlider.setReboundEffect(true);
        pageSlider.setCentralScrollMode(true);
        return component;
    }

    @Override
    public Component makeView(int vertical, int horizontal) {
        Button btn = new Button(this);
        DirectionalLayout.LayoutConfig layoutConfig = new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT,
                DirectionalLayout.LayoutConfig.MATCH_PARENT);
        btn.setLayoutConfig(layoutConfig);
        btn.setText("VERTICAL " + horizontal);
        btn.setTextSize(40);
        Font.Builder builder = new Font.Builder(btn.getText()).setWeight(600);
        btn.setFont(builder.build());
        return btn;
    }
}
