package pro.alexzaitsev.freepager.sample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {
    /**
     * 全UI应用、不支持Context,不支持单元测试
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("pro.alexzaitsev.freepager.sample", actualBundleName);
    }
}