package pro.alexzaitsev.freepager.library.view.infinite;

import ohos.agp.components.Component;

/**
 * @author A.Zaitsev
 */
public interface ViewFactory {

    Component makeView(int vertical, int horizontal);

}