/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package pro.alexzaitsev.freepager.library.view.infinite;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

public class InfiniteVerticalPagerAdapter extends PageSliderProvider {
    private ViewFactory mFactory;
    private int mVerticalPosition;

    public InfiniteVerticalPagerAdapter(ViewFactory factory, int vertical) {
        this.mFactory = factory;
        this.mVerticalPosition = vertical;
    }

    @Override
    public int getCount() {
        return Constants.FIXED_VERTICAl_num;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Component subitem = mFactory.makeView(mVerticalPosition, position);
        componentContainer.addComponent(subitem, 0);
        return subitem;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }

}
