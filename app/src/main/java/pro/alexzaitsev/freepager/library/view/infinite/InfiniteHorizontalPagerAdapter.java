package pro.alexzaitsev.freepager.library.view.infinite;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.PageSliderProvider;

import java.util.List;

public class InfiniteHorizontalPagerAdapter extends PageSliderProvider {
    private List<Component> views;

    public InfiniteHorizontalPagerAdapter(List<Component> views) {
        this.views = views;
    }

    @Override
    public int getCount() {
        return views.size();
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int position) {
        Component component = views.get(position);
        componentContainer.removeComponent(component);
        componentContainer.addComponent(component);
        return component;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object object) {
        return component == object;
    }
}
